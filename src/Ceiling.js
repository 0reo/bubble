import src.CollisionObj;
import math.geom.Rect as Rect;

// import ui.ImageView;
// import ui.resource.Image as Image;


exports = Class(src.CollisionObj, function (supr) {

	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			backgroundColor: '#0000ff'
		});

		supr(this, 'init', [opts]);

		this.type = "ceiling";
		this.children = {
			length:0
		};
		this.body = {};
		this.build();
	};

	this.getContactPosition = function(){
		return this.getPosition().y + this.getPosition().height;
	}

	/*
	move ceiling down
	*/
	this.moveceiling = function() {
		//
	}


	/* Rest the ceiling properties for the next game.
	 */
	this.resetceiling = function () {
		//
	};

	/*
	 * Layout
	 */
	this.build = function () {
	};
});
