/*
 * The game screen is a singleton view that consists of
 * a scoreboard and a collection of molehills.
 */

import animate;
import ui.View;
import ui.ImageView;
import ui.TextView;
import src.Ceiling as Ceiling;
import src.Bubble as Bubble;
import src.Canon as Canon;
import math.util as util;
import math.geom.Point as Point;
import math.geom.Rect as Rect;
import math.geom.intersect as intersect;

/* Some game constants.
 */
// var score = 0,
// 		high_score = 19,
// 		hit_value = 1,
// 		mole_interval = 600,
// 		game_on = false,
// 		game_length = 20000, //20 secs
// 		countdown_secs = game_length / 1000,
// 		lang = 'en';

/* The GameScreen view is a child of the main application.
 * By adding the scoreboard and the molehills as it's children,
 * everything is visible in the scene graph.
 */
exports = Class(ui.ImageView, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 320,
			height: 480,
			image: "resources/images/ui/bg1_center.png"
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	/*
	 * Layout the scoreboard and molehills.
	 */
	this.build = function () {
		/* The start event is emitted from the start button via the main application.
		 */
		this.on('app:start', start_game_flow.bind(this));

		this.ceiling = new Ceiling({width:this.style.width, height: 50});
		this.addSubview(this.ceiling);

		this.maxBubblesRow = 10; //how many rows can we have
		this.maxBubblesCol = 15; //how many columns can we have
		// this.style.width = 320;
		// this.style.height = 480;

		this.bubbleSize = this.style.width / this.maxBubblesCol;


		this._bubbles = [];
		this.canon = new Canon(this.bubbleSize);
		this.addSubview(this.canon);

	};


	this.makeSceneTree = function(bound, start = new Point(0,0), tree = {}, depth=4){

		if (depth > 0 ){
			for (var x = 0; x < 2; x++){
				for (var y = 0; y < 2; y++){
					var label = "" + x + y;


					var newX = ((bound.x - start.x) / 2 * (2-x)) + start.x;
					var newY = ((bound.y - start.y) / 2 * (2-y)) + start.y;

					var startX = newX - ((bound.x - start.x) / 2);
					var startY = newY - ((bound.y - start.y) / 2);

					tree[label] =  new Rect(startX, startY, newX - startX, newY - startY);
					tree[label].depth = depth-1;

					if (depth - 1 == 0){
						tree[label].objs = [];
					}

					var newBound = new Point(newX, newY);
					var newStart = new Point(startX, startY);

					this.makeSceneTree(newBound, newStart, tree[label], depth-1);
				}
			}
		}

		return tree;

	}

	this.addToScene = function(object, tree = this.sceneTree){
		var objRect = new Rect(object.getPosition().x, object.getPosition().y, object.getPosition().width, object.getPosition().height);
		for (pos in tree){
			//what section of the screen are we in?
			if (tree[pos] instanceof Rect && intersect.rectAndRect(objRect, tree[pos])){
				//keep drilling down
				if (tree[pos].depth){
					this.addToScene(object, tree[pos]);
				}	else {
					tree[pos].objs.push(object);
					object.sceneNodes.push(tree[pos]);
				}

			}
		}
	}

	this.populateLevel = function(){


		for (var row = 0, len = this.maxBubblesRow; row < len; row++) {
			var maxCols = this.maxBubblesCol - row % 2;
			for (var col = 0; col < maxCols; col++) {

					if (util.random(1,10) == 5){
						continue;
					}

					var bubbleColor = util.random(0,9);

					var bubble = new Bubble(bubbleColor, this.bubbleSize);
					var xPad = (row % 2) * (bubble.getRadius());

					var yPos =  ( bubble.getRadius() + ((bubble.getRadius()) * Math.sin(0.785398)));

					bubble.style.x = (col * bubble.style.width) + xPad;
					bubble.style.y = (row * yPos) + this.ceiling.style.y + this.ceiling.style.height;
					bubble.row = row;
					this.addSubview(bubble);
					this._bubbles.push(bubble);
		//
		// 			//update score on hit event
		// 			bubble.on('bubble:hit', bind(this, function () {
		// 				if (game_on) {
		// 					score = score + hit_value;
		// 					this._scoreboard.setText(score.toString());
		// 				}
		// 			}));
		// 		}
			}
		}

	}

});




/*
 * Game play
 */

/* Manages the intro animation sequence before starting game.
 */
function start_game_flow () {
	var that = this;

	this.populateLevel();
	this.sceneTree = this.makeSceneTree(new Point(this.getPosition().width, this.getPosition().height));

	this.addToScene(this.ceiling);
	this._bubbles.forEach(function(bubble){
		that.addToScene(bubble, that.sceneTree);
	})
	this.canon.updateQueue(this._bubbles);
	play_game.call(that);


}


/* With everything in place, the actual game play is quite simple.
 * Summon a non-active mole every n seconds. If it's hit, an event
 * handler on the molehill updates the score. After a set timeout,
 * stop calling the moles and proceed to the end game.
 */
 function play_game () {
	 var that = this;

	 GC.app.engine.on('Tick', function (dt) {
	  that._bubbles.forEach(function(b){
			b.checkContacts();
		})
	 });
	//  GC.app.engine.startLoop();
 }
