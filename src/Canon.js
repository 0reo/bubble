import ui.View;
import math.util as util;
import src.Bubble as Bubble;


exports = Class(ui.View, function (supr) {

	this.init = function (bubbleSize) {
		opts = {};

		supr(this, 'init', [opts]);

    this.type = "none";
		this.body = {};
    this.bubbleSize = bubbleSize;
		this.bubbleQueue = [];
		this.build();
	};

	/*
	 * Layout
	 */
	this.build = function () {
	};

  this.updateQueue = function(gameBubbles){
    var availableColors = [];

    gameBubbles.forEach(function(bubble){
      if (availableColors.indexOf(bubble.color) == -1){
        availableColors.push(bubble.color);
      }
    })
    while (this.bubbleQueue.length < 3){
      var bubbleColor = availableColors[util.random(0,availableColors.length - 1)];

      var bubble = new Bubble(bubbleColor, this.bubbleSize);
      var yPos =  0;
      var xPos =  bubble.getRadius() * 2 * this.bubbleQueue.length;
      bubble.style.x = xPos
      bubble.style.y = yPos;
      this.addSubview(bubble);
      this.bubbleQueue.push(bubble);
    }
  }
});
