//sdk imports
import device;
import ui.TextView as TextView;
import ui.StackView as StackView;
//user imports
import src.GameScreen as GameScreen;


exports = Class(GC.Application, function () {

  this.initUI = function () {
    var gamescreen = new GameScreen();

    // Create a stackview of size 320x480, then scale it to fit horizontally
		// Add a new StackView to the root of the scene graph
		var rootView = new StackView({
			superview: this,
			// x: device.width / 2 - 160,
			// y: device.height / 2 - 240,
			x: 0,
			y: 0,
			width: 320,
			height: 480,
			clip: true,
			scale: device.width / 320
		});

    rootView.push(gamescreen);
		gamescreen.emit('app:start');
    /* Listen for an event dispatched by the title screen when
		 * the start button has been pressed. Hide the title screen,
		 * show the game screen, then dispatch a custom event to the
		 * game screen to start the game.
		 */
		// titlescreen.on('titlescreen:start', function () {
		// 	rootView.push(gamescreen);
		// 	gamescreen.emit('app:start');
		// });

		// /* When the game screen has signalled that the game is over,
		//  * show the title screen so that the user may play the game again.
		//  */
		// gamescreen.on('gamescreen:end', function () {
		// 	sound.stop('levelmusic');
		// 	rootView.pop();
		// });

  };

  this.launchUI = function () {

  };

});
