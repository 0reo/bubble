import src.CollisionObj;
import ui.ImageView;
import ui.resource.Image as Image;
import animate;

var imgs = [
	new Image({url: "resources/images/bubbles/ball_blue.png"}),
	new Image({url: "resources/images/bubbles/ball_green.png"}),
	new Image({url: "resources/images/bubbles/ball_purple.png"}),
	new Image({url: "resources/images/bubbles/ball_red.png"}),
	new Image({url: "resources/images/bubbles/ball_yellow.png"})
];

var bubbleSize = 40;

exports = Class(src.CollisionObj, function (supr) {

	this.init = function (colour, size = 40) {
		opts = {
			width:	size,
			height: size
		};

		supr(this, 'init', [opts]);

		this.type = "bubble";
		this.parents = [];
		this.children = {
			// left:null,
			// right:null,
			bLeft:null,
			bRight:null,
			length:0
		};

		this.color = colour;
		this.bubbleSize = size;
		this.active = false;
		this.isFired = false;
		this.row = 9999;
		this.build(colour);
	};

	/*
	fire a bubble up the screen
	*/
	this.fire = function() {

	}


	/*
		make this bubble the child of a bubble or the ceiling
	*/
	this.attachTo = function(newParent){

		switch(newParent.type){
			case "ceiling":
				// if(parent.children.length <= 2){
					newParent.children[newParent.children.length] = this;
					newParent.children.length++;
					this.parents.push(newParent);
				// }
			break;
			case "bubble":

				var onCeiling = this.parents.findIndex(function(parent){
					return parent.type == "ceiling";
				});

				if(onCeiling == -1 &&
					newParent.children.length <= 2){

					newParent.children[this.checkContactPoint(newParent)] = this;
					newParent.children.length ++;
					this.parents.push(newParent);
				}
			break;
		}

	}

	/*
		make sure bubble is connected to the correct side.  return side connected to
	*/
	this.checkContactPoint = function(bubble){

		//what side/angle
		var xAng = this.getPosition().x - bubble.getPosition().x;
		xAng =  xAng >= 0? 45.0 : 135.0;

		//position new bubble if active
		if (this.active){
			var rootXPos = bubble.getPosition().x;
			var rootYPos = bubble.getPosition().y + bubble.getRadius();

			var xPos =  rootXPos + bubble.getRadius() * Math.cos(xAng * Math.PI / 180.0);
			var yPos =  rootYPos + bubble.getRadius() * Math.sin(45.0 * Math.PI / 180.0);

			xPos /= bubble.getPosition().scale;
			yPos /= bubble.getPosition().scale;

			this._animator.clear()
				.now({x: xPos, y: yPos }, 700, animate.easeInCubic);
		}


		return xAng != 45.0 ? "bLeft" : "bRight";
	}

	/*
	are we touching anything?  do we need to add a parent,
	or start falling?
	*/
	this.checkContacts = function(){
		var that = this;

		var contacts = [];
		this.sceneNodes.forEach(function(node){
			node.objs.forEach(function(bubble){
				if (contacts.indexOf(bubble) == -1 && bubble !== that){
					contacts.push(bubble)
				}
			})
		})

		contacts.forEach(function(contact){

			switch (contact.type){
				case "ceiling":
					if (that.getPosition().y <= contact.getContactPosition()) {
						that.attachTo(contact);
					}
				break;
				case "bubble":
					var dx = that.getPosition().x - contact.getPosition().x;
					var dy = that.getPosition().y - contact.getPosition().y;
					var distance = Math.sqrt(dx * dx + dy * dy);
					/*
					attach the bubble we're touching to us,
					but only if we're above it
					'that' will be the parent, 'contact' the child
					*/
					if (distance < that.getRadius() + contact.getRadius() &&
							that.row < contact.row &&
							that.getPosition().y < contact.getPosition().y) {
						contact.attachTo(that);

					}
				break;
			}

		})



		if (!this.parents.length && this.active){
			this.drop();
		}
	}

	/*
	we're not attached to anything!  let the bodies hit the flooooor
	*/
	this.drop = function () {
		this.active = false;

		if (this.children.length){
			if (this.children.bRight)
				this.children.bRight.drop();
			if (this.children.bLeft)
				this.children.bLeft.drop();
		}


		this._animator.clear()
			.now({y: this.style.y + 300}, 700, animate.easeInCubic);
	}

	this.getRadius = function(){
		return this.getPosition().width/2;
	}

	/*
	 * Layout
	 */
	this.build = function (colour) {


		this._bubbleview = new ui.ImageView({
			superview: this,
			image: imgs[colour % imgs.length],
			width: this.bubbleSize,
			height: this.bubbleSize
		});


		// console.log(this.getBoundingShape());

		/* Create an animator object for mole.
		 */
		this._animator = animate(this);
		this._interval = null;

		// var sound = soundcontroller.getSound();

	};
});
