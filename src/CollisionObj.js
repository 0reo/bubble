import ui.View;


exports = Class(ui.View, function (supr) {

	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0
		});

		supr(this, 'init', [opts]);

    this.type = "none";
		this.body = {};
		this.sceneNodes = [];
		this.build();
	};

	/*
	 * Layout
	 */
	this.build = function () {
	};

  this.on("collision", function(contact){
    console.log(this.type + " is being touched by a " + contact.type);
  });
});
